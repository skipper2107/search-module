# Search module

```php
class ArticleSeeker extends AbstractSeeker
{
    public const FILTERS = [
        'page' => PageFilter::class,
    ];
}

class PageFilter implements FilterInterface
{
    public function applyFilter(array $criteria, $value): array
    {
        $criteria['pagination']['limit'] = 20;
        $criteria['pagination']['offset'] = ($value - 1) * 20;

        return $criteria;
    }

    public function getValidationRules(): array
    {
        return [
            new Interger(),
            new GreaterThan(0),
        ];
    }
}

```