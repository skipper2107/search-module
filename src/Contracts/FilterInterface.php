<?php
namespace Skipper\Search\Contracts;

interface FilterInterface
{
    /**
     * @param array $criteria
     * @param $value
     * @return array
     */
    public function applyFilter(array $criteria, $value): array;

    /**
     * @return array
     */
    public function getValidationRules(): array;
}