<?php
namespace Skipper\Search\Contracts;

interface Router
{
    /**
     * @param array $options
     * @return string
     */
    public function createLink(array $options): string;
}