<?php
namespace Skipper\Search\Contracts;

interface ValidatorInterface
{
    /**
     * @param array $options
     * @param array $rules
     * @return array
     */
    public function validate(array $options, array $rules): array;
}