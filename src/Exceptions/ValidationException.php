<?php
namespace Skipper\Search\Exceptions;

use Skipper\Exceptions\DomainException;
use Skipper\Exceptions\Error;
use Throwable;

class ValidationException extends DomainException
{
    /**
     * ValidationException constructor.
     * @param Error[] $errors
     * @param Throwable|null $previous
     */
    public function __construct(array $errors, Throwable $previous = null)
    {
        parent::__construct('Validation Error', 'validation', [
            'errors' => $errors,
        ], $previous, 422);

        foreach ($errors as $error) {
            $this->addError($error);
        }
    }
}