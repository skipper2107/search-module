<?php
namespace Skipper\Search\Services;

use Skipper\Exceptions\Error;
use Skipper\Repository\Contracts\Entity;
use Skipper\Repository\Contracts\Repository;
use Skipper\Repository\CriteriaAwareRepository as CAR;
use Skipper\Search\Contracts\FilterInterface;
use Skipper\Search\Contracts\ValidatorInterface;
use Skipper\Search\Exceptions\ValidationException;
use Skipper\Search\ValueObjects\SearchResult;

abstract class AbstractSeeker
{
    public const DEFAULT_SORT_COLUMN = 'id';
    public const DEFAULT_SORT_ORDER = 'desc';

    public const FILTERS = [];
    public const DEFAULT_OFFSET = 0;
    public const DEFAULT_LIMIT = 20;

    /**
     * @var FilterInterface[]
     */
    protected $filters = [];

    /**
     * @var Repository
     */
    protected $driver;

    /**
     * @var MetaGenerator
     */
    protected $metaGenerator;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function __construct(
        Repository $driver,
        FilterResolver $filterResolver,
        MetaGenerator $metaGenerator,
        ValidatorInterface $validator
    ) {
        $this->driver = $driver;
        foreach (static::FILTERS as $filter => $className) {
            $this->filters[strtolower($filter)] = $filterResolver->resolve($className);
        }
        $this->metaGenerator = $metaGenerator;
        $this->validator = $validator;
    }

    /**
     * @param array $options
     * @param bool $includeMeta
     * @return SearchResult
     * @throws ValidationException
     */
    final public function seek(array $options, bool $includeMeta = false): SearchResult
    {
        $query = $this->getInitialQuery();

        $options = $this->validateOptions($options);

        foreach ($this->filters as $key => $filter) {
            if (array_key_exists($key, $options)) {
                $query = $filter->applyFilter($query, $options[$key]);
            }
        }

        if (empty($query[CAR::SORT] ?? [])) {
            $query[CAR::SORT] = [
                static::DEFAULT_SORT_COLUMN => static::DEFAULT_SORT_ORDER,
            ];
        }

        ['data' => $data, 'total' => $totalCount] = $this->driver->getAllWithTotalCount($query);

        $limit = $query[CAR::PAGINATION][CAR::LIMIT] ?? static::DEFAULT_LIMIT;
        $offset = $query[CAR::PAGINATION][CAR::OFFSET] ?? static::DEFAULT_OFFSET;
        $page = (int)($offset / $limit) + 1;

        $searchResult = new SearchResult(
            $this->modifyResultData($data),
            $limit,
            $page,
            $offset,
            $totalCount
        );

        if ($includeMeta) {
            $searchResult->setMeta($this->metaGenerator->generateMeta($options, $searchResult));
        }

        return $searchResult;
    }

    /**
     * @return array
     */
    protected function getInitialQuery(): array
    {
        return [
            CAR::FILTER => [],
            CAR::SORT => [],
            CAR::PAGINATION => [
                CAR::LIMIT => static::DEFAULT_LIMIT,
                CAR::OFFSET => static::DEFAULT_OFFSET,
            ],
        ];
    }

    /**
     * @param array $options
     * @return array
     * @throws ValidationException
     */
    private function validateOptions(array $options): array
    {
        foreach ($options as $filter => $value) {
            $filterNormalized = strtolower($filter);
            if ($filterNormalized !== $filter) {
                unset($options[$filter]);
                $options[$filterNormalized] = $value;
            }
            if (array_key_exists($filterNormalized, $this->filters)) {
                $rules[$filterNormalized] = $this->filters[$filterNormalized]->getValidationRules();
            }
        }

        $errors = [];
        foreach ($this->validator->validate($options, $rules ?? []) as $field => $message) {
            $errors[] = new Error($message, 'validationError', $field);
        }

        if (false === empty($errors)) {
            throw new ValidationException($errors);
        }

        return $options;
    }

    /**
     * @param Entity[] $data
     * @return Entity[]
     */
    protected function modifyResultData(array $data): array
    {
        return $data;
    }
}