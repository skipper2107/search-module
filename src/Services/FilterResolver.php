<?php
namespace Skipper\Search\Services;

use Skipper\Search\Contracts\FilterInterface;

class FilterResolver
{
    /**
     * @param string $filter
     * @return FilterInterface
     */
    public function resolve(string $filter): FilterInterface
    {
        return new $filter;
    }
}