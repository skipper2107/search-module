<?php
namespace Skipper\Search\Services;

use Skipper\Search\Contracts\Router;
use Skipper\Search\ValueObjects\SearchResult;

final class MetaGenerator
{
    public const PAGE = 'page';
    public const LIMIT = 'limit';
    public const OFFSET = 'offset';

    /**
     * @var Router
     */
    private $router;

    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    /**
     * @param array $options
     * @param SearchResult $result
     * @return array
     */
    public function generateMeta(array $options, SearchResult $result): array
    {
        return [
            'limit' => $result->getLimit(),
            'offset' => $result->getOffset(),
            'page' => $result->getPage(),
            'total' => $result->getTotalCount(),
            'count' => count($result->getData()),
            'prev' => $this->buildLink($options, $result, false),
            'next' => $this->buildLink($options, $result, true),
        ];
    }

    private function buildLink(array $options, SearchResult $searchResult, bool $isNext): ?string
    {
        $offset = $isNext
            ? $searchResult->getOffset() + $searchResult->getLimit()
            : $searchResult->getOffset() - $searchResult->getLimit();
        $result = $isNext ? $offset >= $searchResult->getTotalCount() : $offset < 0;

        if ($result) {
            return null;
        }

        if (($searchResult->getPage() - 1) * $searchResult->getLimit() === $searchResult->getOffset()) {
            $page = (int)($offset / $searchResult->getLimit()) + 1;
            if (1 === $page) {
                $page = null;
            }
            $merge = [
                self::PAGE => $page,
            ];
        } else {
            $merge = [
                self::OFFSET => $offset,
                self::LIMIT => $searchResult->getLimit(),
            ];
        }

        return $this->router->createLink(array_merge($options, $merge));
    }
}