<?php
namespace Skipper\Search\ValueObjects;

use Skipper\Repository\Contracts\Entity;

final class SearchResult
{
    /**
     * @var array
     */
    private $data = [];

    /**
     * @var array
     */
    private $meta = [];

    /**
     * @var int|null
     */
    private $totalCount;

    /**
     * @var int
     */
    private $limit;

    /**
     * @var int
     */
    private $offset;

    /**
     * @var int
     */
    private $page;

    /**
     * SearchResult constructor.
     * @param Entity[] $data
     * @param int $limit
     * @param int $page
     * @param int $offset
     * @param int|null $totalCount
     */
    public function __construct(
        array $data,
        int $limit,
        int $page = 1,
        int $offset = 0,
        ?int $totalCount = null
    ) {
        $this->data = $data;
        $this->limit = $limit;
        $this->page = $page;
        $this->offset = $offset;
        $this->totalCount = $totalCount;
    }

    /**
     * @return Entity[]
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @return array
     */
    public function getMeta(): array
    {
        return $this->meta;
    }

    /**
     * @param array $meta
     */
    public function setMeta(array $meta): void
    {
        $this->meta = $meta;
    }

    /**
     * @return int|null
     */
    public function getTotalCount(): ?int
    {
        return $this->totalCount;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @return int
     */
    public function getOffset(): int
    {
        return $this->offset;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

}